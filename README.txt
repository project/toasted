Toasted is based on Burnt (get it?) by Drupal user der.

Burnt: http://drupal.org/project/burnt
der: http://drupal.org/user/20575


Installation
------------
Unpack the Toasted theme to your site's theme directory
(e.g. /sites/all/themes) as you would unpack any other Drupal theme. After you
unpack the theme, it will be available for selection on the /admin/build/themes
page. To use Toasted, enable it as the default theme on the /admin/build/themes
page.

Disabling Toasted
-----------------
If you later decide to use a different theme, navigate once again to
/admin/build/themes and simply enable a different default theme.

Removal
-------
If you have no intention of using Toasted again, you may wish to remove the
theme completely. This is not necessary, as you could always just enable another
theme as the default theme. However, if you wish to conserve as much web space
as possible, you do have the option of removing Toasted completely.

Before you do so, it is very important that you first enable a different theme
as the default theme. I can't stress that enough. Failing to do so will result
in all sorts of headaches (hint: that's bad). So please be certain that you have
enabled a different default theme and a different administration theme before
you continue.

One a different default theme and a different administration theme have been
enabled, you can remove the toasted folder from your theme directory
(e.g. /sites/all/themes).

Internet Explorer 6
-------------------
I know, I know. No developer is happy to hear those words.

And that is why I didn't bother to support IE6. The way I see it, it's a vicious
cycle. As long as developers continue to develop for IE6, users will continue to
use IE6. It's not going to be easy, but I think it's time developers finally
come together and deliver the final blow. Goodbye IE6, it was a disaster working
with you.

That being said, if you are an enthusiastic developer who can't let go of the
past and has way too much free time on his hands :) feel free to contact us
about collaborating on an IE6 effort. Maybe we could even sign you on as a CVS
contributor. You can reach us at OpenPublishingLab [at] gmail [dot] com

Credits
-------
Toasted was created specifically for use in Innovation News, a web-to-print
newspaper which has been developed by the Open Publishing Lab at the Rochester
Institute of Technology. Innovation News has been imagined and fostered by
the creative minds of Matthew Bernius and Patricia Albanese.

Development was led by by Mark Newell. Mark is also credited with coming up with
the name for this theme, and if I might say so, it is a pretty awesome name.

Additional modifications were provided by John Karahalis.

Open Publishing Lab: http://opl.cias.rit.edu/
Innovation News: http://opl.cias.rit.edu/inews/
Matthew Bernius: http://opl.cias.rit.edu/content/2008/04/04
Patricia Albanese (Pitkin): http://opl.cias.rit.edu/content/2008/04/13-0
Mark Newell: http://opl.cias.rit.edu/content/2008/04/15
John Karahalis: http://opl.cias.rit.edu/content/2008/03/30

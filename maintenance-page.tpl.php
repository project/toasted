<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language ?>" xml:lang="<?php print $language->language ?>">
  <head>
    <title><?php print check_plain($head_title) ?></title>
    <meta http-equiv="Content-Style-Type" content="text/css" />
    <?php print $head ?>
    <?php print $styles ?>
    <?php print $scripts ?>
    <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?></script>
  </head>
<body>
<div id="container">
  <div id="header">
    <a class="no_outline" href="<?php print url() ?>">
    <?php if ($logo) { ?>
      <img id="logo" src="<?php print check_url($logo) ?>" alt="<?php print check_plain($site_name) ?>" />
    <?php } else { ?>
      <span id="site_name"><?php print check_plain($site_name) ?></span>
    <?php } ?>
    </a>
    <?php if ($site_slogan) { ?><div id="site_slogan"><p><?php print check_plain($site_slogan) ?></p></div><?php } ?>
  </div>

  <div id="main">

<?php $content_classes = toasted_content_classes($is_front, $layout) ?>
    <div id="content"<?php print $content_classes ?>>
      <p><?php print $content ?></p>
    </div>
  <div class="float_clear"></div>
  </div>

  <?php if ($footer_message) { ?>
  <div id="footer">
    <p id="footer_message"><?php print $footer_message ?></p>
  </div>
  <?php } ?>

  <?php print $closure ?>    
</div>

</body>
</html>

<?php

/**
 * Define the block HTML.
 *
 * @param $block
 *   The block object.
 * @return
 *   The HTML to display the block.
 */
function toasted_block($block) {
  $output  = "<div class=\"block block-$block->module\" id=\"block-$block->module-$block->delta\">\n";
  // Don't print the header if it's the navigation block.
  if (!($block->module == 'user' && $block->delta == '1')) {
    $output .= " <h2 class=\"title\">$block->subject</h2>\n";
  };
  $output .= " <div class=\"content\">$block->content</div>\n";
  $output .= "</div>\n";
  return $output;
} // function toasted_block

/**
 * Determine if we are in the admin section.
 *
 * @return
 *   TRUE if we are in the admin section.
 */
function toasted_is_admin() {
  if (((arg(0) == 'admin') || (arg(0) == 'sadmin')) && (user_access('access administration pages'))) {
    return true;
  }
} // function toasted_is_admin

/**
 * Compile the classes for the content div.
 *
 * @param $is_front
 *   TRUE if the function is called on the front page.
 * @param $layout
 *   The value of $layout on the calling page.
 * @return
 *   The HTML which will assign the classes, or an empty string.
 */
function toasted_content_classes($is_front, $layout) {
  $result = '';
  $content_classes = array();

  if(toasted_is_admin()) {
    $content_classes[] = 'admin';
  }

  if($is_front) {
    $content_classes[] = 'front';
  }

  if($layout != 'none') {
    $content_classes[] = $layout;
  }

  if(count($content_classes) != 0) {
    $result = ' class="' . implode(' ', $content_classes) . '"';
  }

  return $result;  
} // function toasted_content_classes

/**
 * Compile the classes for the sidebar-right div.
 *
 * @param $is_front
 *   TRUE if the function is called on the front page.
 * @param $layout
 *   The value of $layout on the calling page.
 * @return
 *   The HTML which will assign the classes, or an empty string.
 */
function toasted_right_classes($is_front, $layout) {
  $result = '';
  $right_classes = array();

  if(toasted_is_admin()) {
    $right_classes[] = 'admin';
  }

  if($is_front) {
    $right_classes[] = 'front';
  }

  if($layout != 'none') {
    $right_classes[] = $layout;
  }

  if(count($right_classes) != 0) {
    $result = ' class="' . implode(' ', $right_classes) . '"';
  }
  
  return $result;
} // function toasted_right_classes

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language ?>" xml:lang="<?php print $language->language ?>">
  <head>
    <title><?php print check_plain($head_title) ?></title>
    <?php if ($is_front) { ?><link rel="alternate" type="application/rss+xml" title="Recent News" href="<?php print url(); ?>rss.xml" /><?php } ?>
    <meta http-equiv="Content-Style-Type" content="text/css" />
    <?php print $head ?>
    <?php print $styles ?>
    <?php print $scripts ?>
    <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?></script>
  </head>
<body>
<div id="container">
  <div id="header">
    <a class="no_outline" href="<?php print url() ?>">
    <?php if ($logo) { ?>
      <img id="logo" src="<?php print check_url($logo) ?>" alt="<?php print check_plain($site_name) ?>" />
    <?php } else { ?>
      <span id="site_name"><?php print check_plain($site_name) ?></span>
    <?php } ?>
    </a>
    <?php if ($is_front) { ?><span id="rss"><a href="<?php print url('rss.xml', array('absolute' => 'true')) ?>"><img width="14" height="14" src="/<?php print $directory ?>/images/rss.png" /></a></span><?php } ?>
    <?php if ($site_slogan) { ?><div id="site_slogan"><p><?php print check_plain($site_slogan) ?></p></div><?php } ?>
    <?php if ($search_box) { print $search_box; } ?>
  </div>

  <?php if ($header) { ?><div id="blocks_header"><?php print $header ?><div class="float_clear"></div></div><?php } ?>

  <div id="main">

    <?php if ($mission) { ?><div id="mission"><?php print $mission ?></div><?php } ?>

    <?php if ($left) { ?>
    <div id="sidebar-left"<?php if($is_front) print " class=\"front\""?>>
      <?php print $left ?>
    </div>
    <?php } ?>

<?php $content_classes = toasted_content_classes($is_front, $layout) ?>
    <div id="content"<?php print $content_classes ?>>
      <?php print $breadcrumb ?>
      <?php print $messages ?>
      <?php print $help ?>
      <h1 class="title"><?php print $title ?></h1>
      <div class="tabs"><?php print $tabs ?></div>
      <?php print $content; ?>
    </div>

<?php $right_classes = toasted_right_classes($is_front, $layout) ?>
    <?php if($right) { ?>
    <div id="sidebar-right"<?php print $right_classes ?>>
      <?php print $right ?>
    </div>
    <?php } ?>
    <div class="float_clear"></div>
  </div>

  <?php if ($footer || $footer_message) { ?>
  <div id="footer">
    <?php if ($footer_message) { ?><p id="footer_message"><?php print $footer_message ?></p><?php } ?>
    <?php if ($footer) { ?><div id="blocks_footer"><?php print $footer ?></div><?php } ?>
    <div class="float_clear"></div>
  </div>
  <?php } ?>

  <?php print $closure ?>    
</div>

</body>
</html>
